/**
 * Control software to syncronice my dogs fitbits. The actually sync is
 * is done with galileo (https://bitbucket.org/benallard/galileo).
 *
 * Connected a PIR sensor to GPIO17
 */

#include <stdio.h>    // Used for printf() statements
#include <wiringPi.h> // Include WiringPi library!
#include <stdlib.h>

// For pid locking
#include <sys/file.h>
#include <errno.h>

const int pirPin = 17; // Broadcom pin 17, P1 pin 11
FILE *fh;

void trace(const char *str) {
    // TODO: Timestamp
    fprintf(fh, str);
    fflush(fh);

    printf(str);
}

void traceInt(int value) {
    char str[5] = "";
    snprintf(str, 5, "%d", value);
    trace(str);
}

void run(const char *cmd) {
  FILE *fp;
  char path[1035];

  // Open the command for reading.
  fp = popen(cmd, "r");
  if (fp == NULL) {
    trace("Failed to run command\n" );
    exit(1);
  }

  // Read the output a line at a time - output it.
  while (fgets(path, sizeof(path)-1, fp) != NULL) {
    trace(path);
  }

  pclose(fp);
}

int main(void) {
    // Make sure no other instance is running
    int pidFile = open("/var/run/dogsync.pid", O_CREAT | O_RDWR, 0666);
    int rc = flock(pidFile, LOCK_EX | LOCK_NB);
    if(rc) {
        if(EWOULDBLOCK == errno) {
            // another instance is running
            printf("dogSync: Another instance is already running!!!\n");
            printf("dogSync: exit(1)\n");
            return 1;
        }
    }

    // Create log file
    fh = fopen("dogSync.log", "w+");

    // Init
    trace("Initialize...\n");
    wiringPiSetupGpio();
    pullUpDnControl(pirPin, PUD_UP);
    trace("Started\n");

    // Do the business
    while(1) {
        if (digitalRead(pirPin)) {
            int i;

            trace("Start new sync\n");
            run("./run 2>&1");
            trace("Sync completed... start waiting\n");

            // Wait minimum 15 minutes before try to sync again
            for(i=15; i>0; --i) {
                traceInt(i);
                trace(" minutes left\n");
                delay(60*1000);
            }
            trace("Ready for new sync\n");
        }
        delay(1000);
    }

    return 0;
}
